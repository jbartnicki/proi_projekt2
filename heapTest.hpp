#ifndef _HEAPTEST_HPP_
#define _HEAPTEST_HPP_

#include "heap.hpp"

class HeapTest
{
private:
	void test(bool value, std::string message);
	
public:
	void testAll();

	void testAdd();
	void testRemoveFirst();
	void testRemoveAll();
	void testSum();
	void testDifference();
	void testCompare();
	void testPop();
};

#endif