#ifndef _HEAP_HPP_
#define _HEAP_HPP_

#include <vector>
#include <iostream>
#include <algorithm>

/* Węzły w kopcu indeksowane są wierszami:
 *                    0 
 *                 /    \
 *               1        2
 *             /  \     /  \
 *            3    4   5    6
 */
 
class HeapTest;
 
class Heap
{
private:
	std::vector<int> elements;
	
	// Sprawdzanie czy węzły są poprawnie poustawiane
	bool fixUp(int start);
	bool fixDown(int start);
	
	bool hasValidStructure(size_t start);
	
public:
	void add(int value); // dodaje wartość do kopca
	
	void removeAt(int index); // usuwa węzeł o zadanym indeksie
	bool removeFirst(int value); // usuwa pierwsze wystąpienie danej wartości w kopcu
	void removeAll(int value); // usuwa wszystkie wystapienia wartości
	
	bool hasValidStructure();
	
	inline size_t getIndex(int value) const; // znajduje pierwszy węzeł o zadanej wartości
	int getValue(size_t index) const; // pobiera wartość węzła
	
	int& operator[](size_t index);
	
	// zawieranie wartości
	inline bool contains(int value) const { return std::find(elements.begin(), elements.end(), value) != elements.end(); }
	
	// liczba elementów
	inline size_t size() const { return elements.size(); }
	
	Heap();
	Heap(const Heap& heap);
	
	Heap operator+(const Heap& heap);
	Heap operator-(const Heap& heap);
	
	bool operator==(const Heap& heap);
	bool operator!=(const Heap& heap);
	
	Heap& operator=(const Heap& heap);
	Heap& operator+=(const Heap& heap);
	Heap& operator-=(const Heap& heap);
	
	
	
	int pop(); // zwraca wartość z korzenia kopca i usuwa go
	
	std::ostream& operator<<(std::ostream& os);
	
	//void printAll() const { for(size_t i = 0; i < elements.size(); i++) std::cout << elements[i] << ", "; std::cout << std::endl; }
};

#endif