#include "heapTest.hpp"
#include <iostream>

void HeapTest::test(bool value, std::string message)
{
	std::cout << '\t' << message << " - " << (value ? "SUKCES" : "PORAZKA") << std::endl; 
}

void HeapTest::testAdd()
{
	std::cout << "Test dodawania elementow\n";
	Heap a;
	a.add(7);
	a.add(5);
	
	test(a.hasValidStructure(), "struktura kopca");
	test(a.contains(5) && a.contains(7), "zawieranie elementow");
	test(a.size() == 2, "liczba elementow");
	
	std::cout << std::endl;
}

void HeapTest::testRemoveFirst()
{
	std::cout << "Test usuwania elementu\n";
	
	Heap a;
	a.add(6);
	a.add(16);
	a.add(37);
	
	a.removeFirst(16);
	
	test(a.hasValidStructure(), "struktura kopca");
	test(a.contains(6) && a.contains(37) && !a.contains(16), "zawieranie elementow");
	test(a.size() == 2, "liczba elementow");
	
	std::cout << std::endl;
}

void HeapTest::testRemoveAll()
{
	std::cout << "Test usuwania wielu elementow\n";
	
	Heap a;
	a.add(1000);
	a.add(10);
	a.add(1);
	a.add(100);
	a.add(10);
	a.add(100);
	
	a.removeAll(10);
	
	test(a.hasValidStructure(), "struktura kopca");
	test(a.contains(1000) && a.contains(100) && a.contains(1) && !a.contains(10), "zawieranie elementow");
	test(a.size() == 4, "liczba elementow");
	
	std::cout << std::endl;
}

void HeapTest::testSum()
{
	std::cout << "Test dodawania kopcow\n";
	
	Heap a, b;
	a.add(184);
	a.add(60);
	
	b.add(-70);
	b.add(100);
	
	Heap c = a + b;
	
	test(c.hasValidStructure(), "struktura kopca");
	test(c.contains(184) && c.contains(60) && c.contains(-70) && c.contains(100), "zawieranie elementow");
	test(c.size() == 4, "liczba elementow");
	
	std::cout << std::endl;
}

void HeapTest::testDifference()
{
	std::cout << "Test odejmowania kopcow\n";
	
	Heap a, b;
	a.add(50);
	a.add(121);
	a.add(400);
	a.add(-1000);
	
	b.add(121);
	b.add(-1000);
	b.add(7);
	
	Heap c = a - b;
	
	test(c.hasValidStructure(), "struktura kopca");
	test(c.contains(50) && c.contains(400) && !c.contains(121) && !c.contains(-1000) && !c.contains(7), "zawieranie elementow");
	test(c.size() == 2, "liczba elementow");
	
	std::cout << std::endl;
}

void HeapTest::testCompare()
{
	std::cout << "Test porownania kopcow\n";
	
	Heap a, b, c;
	a.add(5);
	a.add(3);
	a.add(10030);
	
	b.add(5);
	b.add(3);
	b.add(10030);
	
	c.add(5);
	c.add(3);
	c.add(5783);
	
	test(a == b, "rownosc");
	test(a != c, "nierownosc");
	
	std::cout << std::endl;
}

void HeapTest::testPop()
{
	std::cout << "Test pobierania najmniejszego elementu\n";
	
	Heap a;
	a.add(5);
	a.add(3);
	a.add(10030);
	a.add(0);
	
	int val = a.pop();
	
	test(a.hasValidStructure(), "struktura kopca");
	test(a.contains(5) && a.contains(3) && a.contains(10030) && !a.contains(0), "zawieranie elementow");
	test(a.size() == 3, "liczba elementow");
	test(val == 0, "pobrano najmniejszy element");
	
	std::cout << std::endl;
}

void HeapTest::testAll()
{
	testAdd();
	testRemoveFirst();
	testRemoveAll();
	testSum();
	testDifference();
	testCompare();
	testPop();
}