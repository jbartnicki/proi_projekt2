#include "heap.hpp"
#include <stdexcept>

bool Heap::fixUp(int start)
{
	bool swapHappened = false;
	size_t parent;
	
	// Rodzic elementu o indeksie i znajduje się w indeksie (i - 1) / 2.
	// Zamieniaj rodzica z obecnym elementem dopóki rodzic jest większy lub
	// dotarto do korzenia kopca
	while(start > 0 && elements[parent = (start - 1) / 2] > elements[start]) {
		std::iter_swap(elements.begin() + start, elements.begin() + parent);
		start = parent;
		swapHappened = true;
	}
	
	return swapHappened;
}

bool Heap::fixDown(int start)
{
	bool swapHappened = false;
	size_t child;
	
	// Lewe dziecko elementu o indeksie i znajduje się w indeksie 2i + 1, a prawe w 2i + 2.
	// Zamieniaj obecny element z którymś dzieckiem dopóki jest on większy od niego
	// i nie dotarliśmy do końca kopca czyli elementu najbardziej po prawej stronie ostatniego rzędu.
	while(((child = (start << 1) + 1) < elements.size() && elements[child] < elements[start]) || (++child < elements.size() && elements[child] < elements[start])) {
		std::iter_swap(elements.begin() + start, elements.begin() + child);
		start = child;
		swapHappened = true;
	}
	
	return swapHappened;
}


void Heap::add(int value)
{
	elements.push_back(value);
	fixUp(elements.size() - 1);
}

void Heap::removeAt(int index)
{
	elements[index] = elements[elements.size() - 1];
	elements.pop_back();
	if(fixUp(index) == false) {
		fixDown(index);
	}
}

bool Heap::removeFirst(int value)
{
	size_t index = getIndex(value);
	if(index < elements.size()) {
		removeAt(index);
		return true;
	}
	
	// zwracana jest informacja o tym czy usunięto jakiś element
	return false;
}

void Heap::removeAll(int value)
{
	while(removeFirst(value));
}

Heap::Heap()
{
	
}

Heap::Heap(const Heap& heap)
{
	for(size_t i = 0; i < heap.elements.size(); i++) {
		elements.push_back(elements[i]);
	}
}

Heap Heap::operator+(const Heap& heap)
{
	Heap n;
	for(size_t i = 0; i < elements.size(); i++) {
		n.elements.push_back(elements[i]);
	}
	
	for(size_t i = 0; i < heap.elements.size(); i++) {
		n.add(heap.elements[i]);
	}
	
	return n;
}

Heap Heap::operator-(const Heap& heap)
{
	Heap n;
	for(size_t i = 0; i < elements.size(); i++) {
		n.elements.push_back(elements[i]);
	}
	
	for(size_t i = 0; i < heap.elements.size(); i++) {
		n.removeFirst(heap.elements[i]);
	}
	
	return n;
}

inline size_t Heap::getIndex(int value) const
{
	return std::find(elements.begin(), elements.end(), value) - elements.begin();
}

int Heap::getValue(size_t index) const
{
	if(index >= 0 && index < elements.size()) {
		return elements[index];
	}
	
	throw new std::out_of_range("Indeks wykroczył poza rozmiar kopca.");
}

int& Heap::operator[](size_t index)
{
	if(index >= 0 && index < elements.size()) {
		return elements[index];
	}
	
	throw new std::out_of_range("Indeks wykroczył poza rozmiar kopca.");
}

bool Heap::operator==(const Heap& heap)
{
	if(elements.size() != heap.elements.size()) {
		return false;
	}
	
	for(size_t i = 0; i < elements.size(); i++) {
		if(elements[i] != heap.elements[i]) {
			return false;
		}
	}
	
	return true;
}

bool Heap::operator!=(const Heap& heap)
{
	return !(*this == heap);
}

Heap& Heap::operator=(const Heap& heap)
{
	if(&heap == this) {
		return *this;
	}
	
	elements.clear();
	
	for(size_t i = 0; i < heap.size(); i++) {
		elements.push_back(heap.getValue(i));
	}
	
	return *this;
}

Heap& Heap::operator+=(const Heap& heap)
{
	for(size_t i = 0; i < heap.size(); i++) {
		add(heap.getValue(i));
	}
	
	return *this;
}

Heap& Heap::operator-=(const Heap& heap)
{
	for(size_t i = 0; i < heap.size(); i++) {
		removeFirst(heap.getValue(i));
	}
	
	return *this;
}

int Heap::pop()
{
	if(elements.size() == 0) {
		throw new std::out_of_range("Nie można pobrać elementu z pustego kopca.");
	}
	
	int val = elements[0];
	removeAt(0);
	return val;
}

std::ostream& Heap::operator<<(std::ostream& os)
{
	for(size_t i = 0; i < elements.size() - 1; i++) {
		os << elements[i] << ", ";
	}
	os << elements[elements.size() - 1];

	return os;
}

bool Heap::hasValidStructure(size_t start)
{
	size_t left = (start << 1) + 1;
	size_t right = left + 1; 
	
	if(left < elements.size()) {
		if(elements[start] > elements[left] || hasValidStructure(left) == false) {
			return false;
		}
	}
	
	if(right < elements.size()) {
		if(elements[start] > elements[right] || hasValidStructure(right) == false) {
			return false;
		}
	}
	
	return true;
}

bool Heap::hasValidStructure()
{
	return hasValidStructure(0);
}