// Jan Bartnicki - projekt 2.7
// Implementacja klasy realizującej kopiec typu min

#include "heap.hpp"
#include "heapTest.hpp"

int main(int argc, char **argv)
{
	HeapTest t;
	
	t.testAll();
	
	return 0;
}